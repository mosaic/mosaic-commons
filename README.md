# Mosaic

Morphogenesis: Simulations and Analysis in siliCo

https://mosaic.gitlabpages.inria.fr/mosaic-commons

## Getting started



## Getting started
You may want to start by installing and using the `libmamba-solver` for faster dependency resolving:
```shell
conda install -y -n base -c conda-forge conda-libmamba-solver
conda config --set solver libmamba
```

Create a conda environment with all required dependencies with the conda recipe:
```shell
conda env create -f conda/env.yaml
```


## Live-preview Mkdocs documentation
First activate the environment with:
```shell
conda activate mosaic-commons
```
Then you can build the documentation with:
```shell
mkdocs serve
```


## Build Mkdocs documentation
First activate the environment with:
```shell
conda activate mosaic-commons
```
Then you can build the documentation with:
```shell
mkdocs build
```


## [OLD] Build Sphinx documentation
First activate the environment with:
```shell
conda activate mosaic-commons
```
Then you can build the documentation with:
```shell
make html
```