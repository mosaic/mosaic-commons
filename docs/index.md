# Mosaic Team common resources

<figure>
<img src="assets/images/logo_mosaic_vertical_colors_name_and_subtitle.png" width=350>
</figure>



!!! warning "Website under construction"
    This documentation website is still under construction, and some sections might be missing or incomplete.

This website gathers information on the developed scientific libraries and the common good practices adopted by the members of the Mosaic team regarding software development.
Most of it concerns tools for the development, sharing, testing, maintenance and deployability of Python packages.


## Credits

### Development Lead

* Guillaume CERUTTI ![badge_gc](https://img.shields.io/badge/ResearchEngineer-INRAe-green.svg) `<guillaume.cerutti <AT> inria.fr>`
* Jonathan LEGRAND ![badge_jo](https://img.shields.io/badge/ResearchEngineer-CNRS-blue.svg) `<jonathan.legrand <AT> ens-lyon.fr>`

### Contributors

* Guillaume CERUTTI ![badge_gc](https://img.shields.io/badge/ResearchEngineer-INRAe-green.svg) `<guillaume.cerutti <AT> inria.fr>`
* Jonathan LEGRAND ![badge_jo](https://img.shields.io/badge/ResearchEngineer-CNRS-blue.svg) `<jonathan.legrand <AT> ens-lyon.fr>`


## Contributing

Contributions are welcome, and they are greatly appreciated!
Every little bit helps, and credit will always be given.

