# Releasing a Python package

Here we provide guidelines to perform a release of a package, assuming it is in a repository following the [Git Flow Workflow](https://danielkummer.github.io/git-flow-cheatsheet/)