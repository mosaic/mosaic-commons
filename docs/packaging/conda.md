# Packaging as a conda package

Here, we provide basic guidelines to package a Python project as a Conda package.

## Create the `conda/` directory structure

* Create an empty `conda/` directory in the package

```bash
mkdir conda
```

* Create an empty directory for the environment and the build recipe

```bash
cd conda
mkdir env
mkdir recipe
```

## Create the `conda` recipe

To build a package, `conda-build` requires what is called a *recipe* file, named `meta.yaml` ([source](https://conda.io/projects/conda-build/en/latest/resources/define-metadata.html#defining-metadata-meta-yaml)) that contains all the necessary metadata.

!!! warning
    In the next sections, we use `jinja2` to inject variables from the `pyproject.toml`.

!!! info
    If you struggle with some errors, use the `render` tool from conda to get the finalized metadata file.
    For example: 
    ```bash
    conda render conda/recipe/. -c conda-forge
    ```

The documentation explaining how to template your `meta.yaml` file with jinja is [here](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html#templating-with-jinja).

### Build recipe
In `conda/recipe/`, create a `meta.yaml` file with the following content:

* Load the data from the [`pyproject.toml`](package.md)
```yaml
{% set pyproject = load_file_data('../../pyproject.toml', 'toml', from_recipe_dir=True) %}
{% set project = pyproject.get('project', {}) %}
{% set deps = project.get('dependencies', {}) %}
```

* Add the basic package information (using the loaded data)
```yaml
package:
name: {{ project.get('name') }}
version: {{ project.get('version') }}

source:
path: ../..
```

* Add the requirements from the python package
```yaml
requirements:
  build:
    - setuptools
    - python  {{ python }}
  run:
    - python  {{ python }}
    {% for dep in deps %}
    - {{ dep }}
    {% endfor %}
```

!!! warning "Conda dependencies"
    You will have to explicitly add any dependency that is not installable by `pip`
    ```yaml
      run:
        ...
        - fenics =2019.1
        ...
    ```  

* Add additional project info, replacing `<my_repo>` by the name of your repository:
```yaml
about:
  summary: {{ project.get('description') }}
  license: LGPL-3.0-or-later
  license_file: LICENSE.md
  home: https://mosaic.gitlabpages.inria.fr/<my_repo>
  dev_url: https://gitlab.inria.fr/mosaic/<my_repo>
```

### Build variants
Create a `conda_build_config.yaml` file to specify the build variants

* Specify at least the versions of python for which you are building
```yaml
python:
    - 3.9
    - 3.11
```

## Build the recipe locally

* If necessary, install the [`conda build` utilities](https://conda.io/projects/conda-build/en/latest/install-conda-build.html#installing-conda-build) in the `(base)` environment
```bash
conda activate base
conda install conda-build conda-verify
```

!!! info "`(base)` environment"
    Unlike most packages, [it is recommended](https://github.com/conda/conda-build/issues/4995) to install `conda-build` in the `(base)` environment, as it is part of the conda infrastructure, and could avoid build errors.

* Go to your package directory and run the build command
```bash
conda build -c conda-forge conda/
```

!!! info "Extra channels"
    If your build dependencies requires packages on other [conda channels](https://conda.io/projects/conda-build/en/latest/concepts/channels.html) (such as the `mosaic` channel) you will have to add the to the build command
    ```bash
    conda build -c conda-forge -c mosaic conda/
    ```

## Supply a conda environment file

`conda` allows to create an environment from a list of dependencies specified in an [environment file](https://docs.conda.io/projects/conda/en/stable/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file).
This is useful to:

- provide a repeatable environment, to quickly install all the dependencies required to run the scripts from your publication;
- set up test environments, for continuous integration purposes.

### Conda environment file
In `env/`, create a `env.yaml` file with the following content:

* Start with the environment name, here `my_paper`:
```yaml
  name: my_paper
```

* Then set the required channels
```yaml
  channels:
    - conda-forge
    - defaults
```

* Add `python` as a dependency, here we set a min version to `3.93`, and the `conda`-installable dependencies:
```yaml
  dependencies:
    - python >=3.9
    - ...
    - fenics =2019.1
    - ... 
```

* If you have some dependencies that are `pip`-installable only, you can declare them as follows:
```yaml
  dependencies:
    - python >=3.9
    - ...
    - pip
    - pip:
        - pymdown-extensions
        - mkdocs-gen-files
        - mkdocs-jupyter
```

### Use jinja templating

Note that in case of a package with a `pyproject.toml` file, you may use the declared `'requires-python'` using jinja templating as follows:
```yaml
{% set pyproject = load_file_data('../../pyproject.toml', 'toml', from_recipe_dir=True) %}
{% set project = pyproject.get('project', {}) %}

name: my_paper

channels:
- conda-forge
- defaults

dependencies:
  - python {{ project.get('requires-python') }}
  - ...
```