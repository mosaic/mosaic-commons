# Creating a Python package

Here, we provide basic guidelines to set up a Python package in accordance with the [Python Packaging Authority](https://www.pypa.io/en/latest/). The goal is to make a package that will be conveniently installed locally using `pip` and imported from any location. 

For more detailed information you can refer to the [Python Packaging User Guide](https://packaging.python.org/en/latest/)

## Set up a [`src/` layout](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout)

* Create an empty package directory with your package name

* Create a `src/` directory, with an empty directory with your `package_name/`

* Create a `README.md` file with the minimal information:
    * package name
    * short description of functionalities
    * list of authors / contributors
    * installation procedure

* Add a `LICENSE` file copying the text of the chosen license (typically [LGPL-3.0](https://opensource.org/licenses/LGPL-3.0>))

## Write your [`pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/)

* Fill in the basic information of your package

```toml
[project]
name = "package_name"
version = "0.1.0"
description = "Short description of the package main functionalities"
authors = [
    {name="First Author", email="first.author@email.com"},
]
maintainers= [
    {name="First Author", email="first.author@email.com"},
]
readme = "README.md"
license = {file = "LICENSE"}
requires-python = ">=3.9"
```

* Set the build backend to `setuptools`

```toml
[build-system]
requires = ["setuptools >= 61.0"]
build-backend = "setuptools.build_meta"
```

!!! question "Configure setuptools to look in your source directory"

    ```toml
    [tool.setuptools.packages.find]
    where = ["src"]
    include = ["package_name"]
    ```

### Manage the dependencies of your package

* Provide the main dependencies in the `[project]` section

```toml
[project]
dependencies = [
  "numpy < 2",
  "scipy",
  "matplotlib == 3",
]
```

* Add optional dependencies, for instance for [unit tests](test.md) 
=== "PyTest"
    ```toml
    [project.optional-dependencies]
    test = [
      "pytest",
      "pytest-cov",
    ]
    ```
=== "Nose2"
    ```toml
    [project.optional-dependencies]
    test = [
      "nose2[coverage]",
      "coverage[toml]",
    ]
    ```

* Add information about the [`urls` associated with the project]

```toml
[project.urls]
Homepage = "https://example.com"
Documentation = "https://readthedocs.org"
Repository = "https://github.com/me/spam.git"
Issues = "https://github.com/me/spam/issues"
Changelog = "https://github.com/me/spam/blob/master/CHANGELOG.md"
```

## Transfer the sources in `src/`

* Make sure to add an empty `__init__.py` in each directory

## Install your package

* Install in edit mode with the ``-e`` option to be able to use source updates without re-installing:

```bash
python -m pip install -e .
```

!!! info "Install the package with test dependencies"

    ```bash
    python -m pip install -e ".[test]"
    ```

* Test that the import is working (replacing `package_name` by its actual name):

```bash
python -c "import package_name"
```
