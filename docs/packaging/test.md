# Unit testing

## Create the tests

### Add a test directory to host unit tests

* Create a `test/` directory at the root of the package

### Create a test python module for each module of the package
* Create a Python file named `test_$$$.py` in `test/`

* Create a test class that inherits from  [unittest.TestCase](https://docs.python.org/3/library/unittest.html#unittest.TestCase)

* Define the `setUp` and `tearDown` functions

```python
import unittest

class Test$$$(unittest.TestCase):

    def setUp(self):
        """
        Define class members that will be used for all tests
        """
        pass

    def tearDown(self):
        """
        Cleanup after the execution of every test method
        """
        pass
```

* Write one test method (beginning by `test_`) per testing case

```python
    def test_$$$(self):
        """
        Run some functions of the package
        """

        assert #Some condition
```

* You can use the built-in methods from the `unittest.TestCase` class (`self.assertAlmostEqual`...)

## Run the tests

=== "PyTest"
    * Add the `pytest` testing suite, with the coverage extension, to the test dependencies of your project
    ```toml
    [project.optional-dependencies]
    test = [
      "pytest",
      "pytest-cov",
    ]
    ```
    * Add the test suite configuration in the `pyproject.toml`: avoid to count useless .py files in coverage stats
    ```toml
    [tool.pytest.ini_options]
    testpaths = ["test"]
    addopts = "--cov --cov-report term-missing"

    [tool.coverage.run]
    source = ["src"]
    omit = ["*__init__.py", "test/*", "setup.py"]

    [tool.coverage.report]
    omit = ["*__init__.py", "test/*", "setup.py"]
    ```
    * Install the package with the test dependencies
    ```shell
    python -m pip install -e ".[test]"
    ```
    * At the root of the package, run the testing command to see the percentage of coverage
    ```shell
    pytest -v
    ```

=== "Nose2"
    * Add the `nose2` testing suite and the `coverage` package to the test dependencies of your project
    ```toml
    [project.optional-dependencies]
    test = [
      "nose2",
      "coverage",
    ]
    ```
    * Add the coverage configuration lines in the `pyproject.toml`: avoid to count useless .py files in coverage stats
    ```toml
    [tool.coverage.run]
    source = ["src"]
    omit = ["*__init__.py", "test/*", "setup.py"]

    [tool.coverage.report]
    omit = ["*__init__.py", "test/*", "setup.py"]
    ```
    * Add a `unittest.cfg` file at the root of the package
    ```toml
    [unittest]
    test-file-pattern = test_*.py
    
    [coverage]
    always-on = True
    ```
    * Install the package with the test dependencies
    ```shell
    python -m pip install -e ".[test]"
    ```
    * At the root of the package, run the testing command to see the percentage of coverage
    ```shell
    nose2 -v
    ```
