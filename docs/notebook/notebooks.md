# Notebooks & Git

Jupyter notebooks can be used as part of your documentation as examples, tutorials, ...
This [guide notebook](NotebookIntegration.ipynb) is a good example.

It is advised to clean then before commit as explained in this [medium](https://medium.com/somosfit/version-control-on-jupyter-notebooks-6b67a0cf12a3) post.

When being part of the documentation, cleaned notebooks can be executed by CI jobs.

!!! warning
    CI runners often have limited amount of resources (CPU & RAM), so plan ahead to avoid crashing it!

The trick is to define pre-commit hooks, these are scripts executed prior to commit.

!!! info
    By default ``git init`` will create sample files for each git hook available for use.
    To enable the hooks we can just remove the ``.sample`` extension of each file.

## Create the pre-commit file

From the root of your repository, we start by creating an executable `pre-commit` file as follows:
```bash
touch .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```
###
!!! info
    An example of pre-commit hooks can be found under ``.git/hooks/pre-commit.sample``.


## Remove space from file names prior to commit

We **highly recommend** that you remove any space from the name of the files you commit!

You can do it with the following code:
```bash
parse_git_changes
for ((i = 0; i < ${#changed_files[@]}; i++))
do
   file="${changed_files[$i]}"
   if [[ $file = *" "* ]]
   # If there is a space in the file name, proceed to renaming:
   then
       # Define the new file name with underscore(s) instead of space(s)
       new_file="${file// /_}"
       echo -e "${WARNING}Renaming '${file}' to '${new_file}'..."
       # Rename the file with git:
       git mv "${file}" "${new_file}"
       # Add the renamed file to the list of file to commit:
       git add "${new_file}"
   fi
done
```

!!! info
    This script use some extra variables for printing purposes, it is detailed [here](#color-and-messages).
    It also uses the ``parse_git_changes`` bash function detailed [here](#parse-stage-files).
    Do not forget to add them both to the ``pre-commit`` file before the renaming script.


## Cleaning notebook prior to commit

To avoid committing the changes in output cells to the repository, we should clean them prior to commit operation.
As we may forget to do this manually, we use the following method with git hooks.

```bash
COMMAND='jupyter-nbconvert'
if ! command -v ${COMMAND} &> /dev/null
then
   echo -e "${ERROR}${COMMAND} could not be found!"
   echo -e "${INFO}Install it with 'python -m pip install nbconvert' or 'conda install nbconvert'"
   # Abort if convert command is not found!
   exit 1
else
  parse_git_changes
  for ((i = 0; i < ${#changed_files[@]}; i++))
  do
     file="${changed_files[$i]}"
     # Extract file 'extension':
     ext="${file##*.}"
     if [[ ${ext} = "ipynb" ]]
     # Process only jupyter notebook files:
     then
        echo -e "${INFO}Processing ${file}"
        jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace "${file}"
        git add "${file}"
     fi
  done
fi
```


!!! info
    This script use some extra variables for printing purposes, it is detailed [here](#color-and-messages).
    It also uses the ``parse_git_changes`` bash function detailed [here](#parse-stage-files).
    Do not forget to add them both to the ``pre-commit`` file before the renaming script.


## Extras

### Color and messages
The coloring variable are defined as follows:
```bash
RED="\033[0;31m"
YELLOW="\033[0;33m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color
INFO="${GREEN}INFO${NC}    "
WARNING="${YELLOW}WARNING${NC} "
ERROR="${RED}ERROR${NC}   "
```


### Parse stage files
The bash function to parse git changes is defined as follows:
```bash
parse_git_changes (){
    changed_files=()
    while read ADDR
    do
        changed_files+=( "$ADDR" )
    done  < <(git diff --cached --name-status --diff-filter=d | awk 'BEGIN { FS = "\t" } ; { if ( /^[^R]/ ) { print $2 } else { print $3 } }')
#    echo -e "${INFO}Found ${#changed_files[@]} changed files in git..."
}
```

Explanations:

1. About the ``git diff`` command:

    - ``--cached``: return the changes between the index and your last commit. See the [git-diff](https://git-scm.com/docs/git-diff#_examples) documentation.
    - ``--name-status``: show only names and status of changed files. See the [git-diff](https://git-scm.com/docs/git-diff#Documentation/git-diff.txt---name-status) documentation.
    - ``--diff-filter=d``: exclude deleted files from the returned list of "changed files". See the [git-diff](https://git-scm.com/docs/git-diff#Documentation/git-diff.txt---diff-filterACDMRTUXB82308203) documentation.

2. About the ``awk`` command:

    - ``BEGIN { FS = "\t" }``: begin by declaring the Field Separator (`FS`) as tabulation (`\t`)
    - ``/^[^R]/``: test if the line start with ANYTHING BUT the letter 'R' ('R' mean that the file has been declared as 'renamed' by ``git diff``)
    - ``{ print $2 } else { print $3 }``: if the file is declared as anything except 'renamed', return the second element else the third (the renamed file)


## Complete example

Hereafter we give a complete example of the ``pre-commit`` file:
```bash
#!/bin/bash

# - Defines colors and message types:
RED="\033[0;31m"
YELLOW="\033[0;33m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color
INFO="${GREEN}INFO${NC}    "
WARNING="${YELLOW}WARNING${NC} "
ERROR="${RED}ERROR${NC}   "

# - Parse the list of staged files as an array of file names:
parse_git_changes (){
    changed_files=()
    while read ADDR
    do
        changed_files+=( "$ADDR" )
    done  < <(git diff --cached --name-status --diff-filter=d | awk 'BEGIN { FS = "\t" } ; { if ( /^[^R]/ ) { print $2 } else { print $3 } }')
#    echo -e "${INFO}Found ${#changed_files[@]} changed files in git..."
}


# -----------------------------------------------------------------------------
# STEP1: rename files to replace whitespaces with underscores:
# -----------------------------------------------------------------------------
parse_git_changes
for ((i = 0; i < ${#changed_files[@]}; i++))
do
    file="${changed_files[$i]}"
    # If there is a space in the file name, proceed to renaming:
    if [[ $file = *" "* ]]
    then
        # Define the new file name with underscore(s) instead of space(s)
        new_file="${file// /_}"
        echo -e "${WARNING}Renaming '${file}' to '${new_file}'..."
        # Rename the file with git:
        git mv "${file}" "${new_file}"
        # Add the renamed file to the list of file to commit:
        git add "${new_file}"
    fi
done

# -----------------------------------------------------------------------------
# STEP2: Clean jupyter notebook outputs:
# -----------------------------------------------------------------------------
COMMAND='jupyter-nbconvert'
if ! command -v ${COMMAND} &> /dev/null
then
    echo -e "${ERROR}${COMMAND} could not be found!"
    echo -e "${INFO}Install it with 'python -m pip install nbconvert' or 'conda install nbconvert'"
    # Abort if convert command is not found!
    exit 1
else
    parse_git_changes
    for ((i = 0; i < ${#changed_files[@]}; i++))
    do
        file="${changed_files[$i]}"
        # Extract file 'extension':
        ext="${file##*.}"
        if [[ ${ext} = "ipynb" ]]
        # Process only jupyter notebook files:
        then
            echo -e "${INFO}Processing ${file}"
            jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace "${file}"
            # Add the clean notebook to the list of file to commit:
            git add "${file}"
        fi
    done
fi
```