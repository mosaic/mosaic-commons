# Mosaic python packages & scientific libraries

The Mosaic team aims at providing scientific packages and libraries.
Most of them are developed in Python and make use of some of the most widely used third-party libraries such as NumPy & Matplotlib.


## Introduction

Hereafter we introduce the scientific libraries developed by the team and their main goal:

* `cell_fem`: **CellFEM** is ...
* `cell_mesh`: **CellMesh** is ...
* `ctrl`: **CTRL** stands for Cell Tracking and Robust Lineaging and is a library dedicated to lineage definition from time-series of tissue image.
* `gnomon`: **Gnomon** is a meta platform dedicated to image analysis and simulation.
* `timagetk`: **TimageTK** (Tissue Image Toolkit) is a Python package dedicated to image processing of multicellular architectures such as plants or animals, and is intended for biologists, modelers and computer scientists.
* `draco_stem`: **Draco Stem** is a library for the reconstruction of 3D meshes from segmented shoot apical meristem (SAM) tissue images : Dual Reconstruction by Adjacency Complex Optimization (DRACO) and SAM Tissue Enhanced Mesh (STEM).
* `tissue_2d`: **Tissue2D** is a package to compute growth, cell division, cell wall stiffening and mechanical equilibrium of plant tissue cross sections formalized as 2D cell complexes.
* `tissue_paredes`: **Tissue Paredes** implement Polarity Analysis through Robust Extraction and Density Estimation of Signals at cell-wall level.
* `tissue_nukem_3d`: **Tissue Nukem3D** is a set of tools to performs nuclei detection.
* `cell_complex`: **CellComplex** implement data structures and algorithms for the representation and manipulation of cellular complexes.
* `tissue_analysis`: **TissueAnalysis** implement algorithms to extract geometrical features from tissue data structures.
* `treex`: **Treex** is a library for manipulating trees and DAG reduction of trees.

## Dependencies

You can get an overview of our libraries dependencies with this image:

![](assets/images/MosaicPackages.png)


## Detailed description

### TimageTK
Contains the **tissue image** data structure with associated algorithms and IO functions.
Algorithms may range from filters to registration and segmentation methods.
A common use is to input confocal microscopy images and returns a cell based segmented tissue.

More info on the TimageTK [website](https://mosaic.gitlabpages.inria.fr/timagetk/).


### TissueAnalysis
Contains the **tissue graph** data structure (MISSING), geometrical features and IO functions.

