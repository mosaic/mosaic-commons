# Creating a `mkdocs` site

## Installing `mkdocs` and extensions

```bash
pip install mkdocs mkdocs-material
```

## Create `mkdocs` project

```bash
mkdocs new .
```

## Fill the `mkdocs.yml`

```yaml
site_name: Mosaic Commons
site_description: Mosaic Team common resources
site_author: MOSAIC
site_url: https://mosaic.gitlabpages.inria.fr/mosaic-commons/

nav:
    - 'Home': 'index.md'
    - 'index.md'

theme:
    name: material
    logo: "assets/images/logo_mosaic_vertical_white_name_and_subtitle.png"

markdown_extensions:
    - attr_list
    - md_in_html
    - pymdownx.emoji:
        emoji_index: !!python/name:material.extensions.emoji.twemoji
        emoji_generator: !!python/name:material.extensions.emoji.to_svg
    - pymdownx.superfences
    - pymdownx.tabbed:
        alternate_style: true

extra_css:
    - assets/css/style.css
```

## Serve your site

```yaml
mkdocs serve
```

## Configure auto docstring parsing

### Install dependencies

```bash
pip install 'mkdocstrings[python]'
pip install pymdown-extensions
pip install mkdocs-gen-files
pip install mkdocs-literate-nav  mkdocs-section-index
```

### Prepare generating script

Create `gen_ref_pages.py` in the `docs` folder with the following content:

```python
"""Generate the code reference pages."""

from pathlib import Path

import mkdocs_gen_files

nav = mkdocs_gen_files.Nav()

root = Path(__file__).parent.parent
src = root / "src"

for path in sorted(src.rglob("*.py")):
    module_path = path.relative_to(src).with_suffix("")
    doc_path = path.relative_to(src).with_suffix(".md")
    full_doc_path = Path("../reference", doc_path)

    parts = tuple(module_path.parts)

    if parts[-1] == "__init__":
        parts = parts[:-1]
        doc_path = doc_path.with_name("index.md")
        full_doc_path = full_doc_path.with_name("index.md")
    elif parts[-1] == "__main__":
        continue

    with mkdocs_gen_files.open(full_doc_path, "w") as fd:
        identifier = ".".join(parts)
        print("::: " + identifier, file=fd)

    mkdocs_gen_files.set_edit_path(full_doc_path, path.relative_to(root))

with mkdocs_gen_files.open("../reference/SUMMARY.md", "w") as nav_file:
    nav_file.writelines(nav.build_literate_nav())
```

See [mkdocstrings documentation page](
https://mkdocstrings.github.io/recipes/) for the complete details

### Enable plugins in `mkdocs.yml`

```yaml
plugins:
    - search
    - literate-nav:
          nav_file: SUMMARY.md
    - section-index
    - mkdocstrings:
          handlers:
              python:
                  paths: [src]  # search packages in the src folder
                  import:
                      - https://docs.python-requests.org/en/master/objects.inv
    - gen-files:
          scripts:
              - docs/gen_ref_pages.py
```

### Add the reference to your `nav`

```yaml
    - 'Reference API': reference/
```