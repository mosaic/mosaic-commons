"""Generate the code reference pages."""

from pathlib import Path

import mkdocs_gen_files

root = Path(__file__).parent
examples_dir = root / 'examples'

md = "# Various examples\n\n"

md += "## Notebooks\n"
for path in sorted(examples_dir.rglob("*.ipynb")):
    md += f"- [{path.name}]({path.relative_to(examples_dir)})\n"

with mkdocs_gen_files.open(examples_dir / "index.md", "w") as index_file:
    index_file.writelines(md)
