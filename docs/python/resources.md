# Resources

Hereafter you will find some links to useful Python resources:

- https://learn.scientific-python.org/development/
- https://setuptools.pypa.io/en/latest/index.html
- https://packaging.python.org/en/latest/