# Python packages

In this section we provide some help and insights about the Python package we often use in the Mosaic team.

Here is a non-exhaustive list of AWESOME Python libraries:

Scientific:

- [NumPy](https://numpy.org/doc/stable/)
- [SciPy](https://docs.scipy.org/doc/scipy/)
- [Scikit-Image](https://scikit-image.org/)
- [Scikit-Learn](https://scikit-learn.org/stable/)
- [Scikit-Spatial](https://scikit-spatial.readthedocs.io/en/stable/)


Visualization:

- [Plotly](https://plotly.com/python/)
- [PyVista](https://docs.pyvista.org/version/stable/)
- [Matplotlib](https://matplotlib.org/stable/)


Web App:

- [Dash](https://dash.plotly.com/)
- [Flask](https://flask.palletsprojects.com/en/3.0.x/)