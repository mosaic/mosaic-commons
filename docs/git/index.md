# Git

## Install Git

=== "conda"
    ```bash
      conda install -n base git
    ```
=== "Linux"
    ```bash
      sudo apt install git
    ```
=== "Mac OS"
    ```bash
      brew install git
    ```

### Install a Git GUI client

=== "Linux"
    ```bash
    sudo apt install gitk
    ```
=== "Mac OS"
    * Install [`gitup`](https://gitup.co/)


## Configure Git

<div class="grid cards" markdown>

- **User details**

    ---

    === ":material-account:{ .lg .middle }"
        ```bash
        git config --global user.name "Firstname Name"
        ```
    === ":material-email:{ .lg .middle }"
        ```bash
        git config --global user.email "email.address@inria.fr"
        ```

- **Core editor**

    ---

    === "Linux"
        ```bash
        git config --global core.editor "gedit --wait"
        ```
    === "Mac OS"
        ```bash
        git config --global core.editor "open -a TextEdit --wait"
        ```
    === "Atom"
        ```bash
        git config --global core.editor "atom --wait"
        ```

- **Initial branch**

    ---

    ```bash
    git config --global init.defaultBranch main
    ```

- **"Tree" view in terminal**

    ---

    ```bash
    git config --global alias.tree "log --format=format:\"%C\(auto\)%h %D %C\(green\)%aN%Creset %s\" --graph --all"
    ```

</div>


## Create a new Git repository

### Initialization

* Put the files you want to version in a same folder

* Go to the folder in your terminal

* Initialize an empty Git repository with `git init`

* Monitor the status of the repository (untracked files) with: `git status`


### Prepare your first commit

* Add the files you want to commit

    ```bash
    git add file1.py
    git add path/to/file2.py
    ```
* Create a `.gitignore` file, see [here](gitlab.md/#example-gitignore) for a complete example:

    ```bash
    gedit .gitignore
    ```

* Add .gitignore to the commit

    ```bash
    git add .gitignore
    ```

* Monitor the content of your commit (green files)

    ```bash
    git status
    ```

### Commit!

* Commit your modifications with an informative message

    ```bash
    git commit -m "First commit: (description of the commit content)"
    ```


## Work on your local Git repository

### Commit your modifications

* Look at all the changes you have made since the last commit with `git diff` or for a specific file `git diff file1.py`

* Add the files you want to commit: `git add file1.py`

* Commit your modifications with an informative message: `git commit -m "Description of the commit content"`

* If no new files are to be added to the commit, you can add all your modifications to tracked files to the commit: `git commit -am "Description of the commit content"`

### Remove modifications

* Discard all modifications on all files: `git stash && git stash drop`

* Stop tracking a given file: `git rm --cached path/to/file.py`

Create a new branch
-------------------

* See which branch you are currently on: `git branch -v`


* Create a new branch named "develop" and move to it: `git checkout -b develop`


* Commit some modifications of the develop branch: `git commit -am "Description of the commit content"`


* Come back to the previous state by switching back to the "main" branch: `git checkout main`

### Delete a branch

To delete a branch, use: `git branch -D develop`

### Merge changes into a branch

* Switch to the branch in which you want to integrate the changes: `git checkout main`

* To avoid conflicts, you should make sure that the branch to merge issues from the current branch

* Merge the commits of the branch to merge into the current branch: `git merge --no-ff develop`

* Leave the merge message as it is by closing the file in your editor

### Solving conflicts

* The merge command stops indicating the files in conflict

* Handle the conflicts in each file in an editor

* Add all the files once conflicts have been handled: `git add file_in_conflict`

* Continue the merge command: `git merge --continue`

* Leave the merge message as it is by closing the file in your editor


## Host your repository on GitLab

### Connect on gitlab.inria.fr

* Use your Inria credentials (even for the first time)

* Request access to the Mosaic group if necessary


### As a personal repository

* Go to your personal projects (Menu/Profile/Personal Projects/View all)

* Create a new empty project (do not tick the README checkbox)

* Add the newly created project as the default remote for your repository (using SSH!)

    ```bash
    git remote add origin git@gitlab.inria.fr:username/project_name.git
    ```

* For every branch you want to host, push it to the remote

    ```bash
    git checkout main
    git push --set-upstream origin main
    ```

## Contribute to an existing Git project

### Get a local copy of the repository

* Open the project home page on Gitlab

* In a terminal go to the folder where you want to copy the project

* Clone the repository by getting the SSH address from the clone button

    ```bash
    git clone git@gitlab.inria.fr:groupname/project_name.git
    ```

* Go to the repository and checkout the desired branch

    ```bash
    cd project_name
    git checkout develop
    ```


### Create a feature branch in which to commit your changes

* Create the feature branch

    ```bash
    git checkout -b feature/feature_name
    ```

* Commit your modifications

    ```bash
    git add path/to/file1.py
    git commit -m "Description of the commit content"
    ```

* Push the new branch

    ```bash
    git push --set-upstream origin feature/feature_name
    ```


### Get the current remote version

* Go to your feature branch

    ```bash
    git checkout feature/feature_name
    ```

* See if the remote repository has changes

    ```bash
    git fetch
    ```

* Get the current version of your branch from the remote repository

    ```bash
    git pull
    ```

* If you have uncommitted modifications, you will need to make a commit first

!!! note
    In such a case, you should rather use git fetch && git rebase instead of git pull to avoid creating an unnecessary merge commit.


### Push your current version to the remote

* Make some commits

* Push the branch

    ```bash
    git push feature/feature_name
    ```

* If the remote branch has new commits, you will need to fetch and rebase (or pull) the modifications first

### Merge the feature branch into a stable branch

* [Open a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/#create-a-merge-request) on the Gitlab repository, making sure to select the right source and target branches

* Have another developer of the project review your feature!

* When everything is validated, press the Merge button

!!! info
    If the feature branch has conflicts with the target branch, the Merge button won't be available.
    You can use the Solve Conflicts button that will perform a merge of the target branch into the source branch in which you will be able to choose "Ours" (source branch) or "Theirs" (target branch) version for each conflicting section, or even manually correct them using the Inline editor.

## More info

Git [Cheatsheet](https://www.atlassian.com/dam/jcr:8132028b-024f-4b6b-953e-e68fcce0c5fa/atlassian-git-cheatsheet.pdf)
