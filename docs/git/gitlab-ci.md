# Gitlab CI


## Custom Docker images

To speed up the CI/CD processes we propose two custom Docker images, based on official `continuumio/miniconda3` image, with the necessary packages installed to build, test & publish conda packages:

* `inriamosaic/conda`: use faster `libmamba` as default `solver`
* `inriamosaic/mamba`: has `mamba` package installed

Corresponding `Dockerfile` can be found in this repository under `docker/conda/Dockerfile` and `docker/mamba/Dockerfile`.


## Configure CI/CD on GitLab

* Click on the **Set up CI/CD** button

* Edit `.gitlab-ci.yml`

* In the text editor paste the following lines to run a dummy `pages` job using the `inriamosaic/mamba` image

    ```yaml
    image: inriamosaic/mamba

    pages:
      stage: deploy
      tags:
        - ci.inria.fr
        - large
      script:
      - echo doc
      artifacts:
        paths:
        - public
      only:
      - main
      - develop
    ```

* Commit the changes.

* Activate pipelines in project Settings

  * Settings / General / Visibility, project features, permissions
  * Toggle the **CI/CD** item (keep the "Only Project Members")
  * Save changes

* Activate runners for the project

  * Settings / CI/CD / Runners
  * Scroll down to the **Shared runners** section
  * Toggle the "Enable shared runners for this project" item


## Configure the Documentation job

* Pull the repository to modify the `.gitlab-ci.yml` (on **main**)

* Add a `pages` entry that will compile the Sphinx doc and add the generated doc as an artifact.

    ```yaml
    pages:
      stage: deploy
      tags:
        - ci.inria.fr
        - large
      script:
        - mamba env create -y -n env-name -f conda/env.yaml
        - source activate env-name
        - mamba install -c conda-forge -y sphinx myst-parser make
        - pip install sphinx-press-theme
        - pip install -e .
        - cd doc
        - make html
        - mv build/html ../public
      artifacts:
        paths:
        - public
      only:
      - main
      - develop
    ```

* Replace the `env-name` by the name of the environment in `env.yaml`

* Commit and push your changes


## Configure the Test/Coverage job

* Modify the `.gitlab-ci.yml` (on **main**)

* Add a `coverage` entry before `pages` that will compile the Sphinx doc and add the generated doc as an artifact.

    ```yaml
    coverage:
      stage: test
      tags:
        - ci.inria.fr
        - large
      script:
      - conda env create -y -f conda/env.yaml
      - source activate env-name
      - python setup.py develop
      - pytest -v --cov=package-name
      only:
      - main
      - develop
    ```

* Replace the `env-name` by the name of the environment in `env.yaml`

!!! info
    If your package relies on :code:`vtk` or other third-party libraries involving rendering for the tests, you should modify the test job to have it run a virtual X server of the CI machines.
    ```yaml
    coverage:
      stage: test
      tags:
        - ci.inria.fr
        - large
      script:
        - apt update
        - apt install -y libgl1 xpra xserver-xorg-video-dummy
        - conda env create -y -f conda/env.yaml
        - source activate env-name
        - python setup.py develop
        - xpra --xvfb="Xorg +extension GLX -config my.xorg.conf -logfile ${HOME}/.xpra/xorg.log"  start :9
        - env DISPLAY=:9 nosetests -v
      only:
      - main
      - develop
    ```

* Commit and push your changes


## Configure the Conda/Anaconda job

* Modify the `.gitlab-ci.yml` (on **main**)

* Add a `conda_build` entry before `coverage` that will build the conda package and store it as an artifact.

    ```yaml
    conda_build:
      stage: build
      tags:
        - ci.inria.fr
        - large
      script:
        - conda install -y git
        - mkdir conda-bld
        - conda build . --output-folder conda-bld/
      artifacts:
        paths:
          - conda-bld
      only:
        - main
        - develop
    ```

!!! info
    If your packages depend on third-party packages that are not on the `defaults` conda channel, you should add the channels to the :code:`conda build` command of the job, for instance:
    ```bash
    conda build -c mosaic . --output-folder conda-bld/
    ```

* Create an account on [anaconda.org](https://www.anaconda.org) and get access to the MOSAIC group.

* Add a token to the CI variables to grant the right to GitLab to upload on [anaconda-cloud](https://www.anaconda.org).

  * Settings / CI/CD / Variables
  * Add a new variable named `ANACONDA_TOKEN` : Type : `Variable`, Key : `ANACONDA_TOKEN`.
  * Get the Value of the variable on the anaconda Mosaic account:
    * Go to [anaconda-cloud](https://www.anaconda.org) and switch to the MOSAIC group in the user menu.
    * Go the Settings / Access in the menu.
    * Scroll down to the list of exi        sting tokens.
    * Click on the **view** button right to the **gitlab** token.
    * Copy the value of the token and paste it in the Value field of the GitLab variable .

  * In `.gitlab-ci.yml`, add a `anaconda` entry after `pages` that will convert the conda package for macOS and Windows, and upload both on anaconda cloud

    ```yaml
    anaconda:
      stage: deploy
      script:
      - conda convert -p osx-64 conda-bld/linux-64/package-name-* --output-dir conda-bld/
      - conda convert -p win-64 conda-bld/linux-64/package-name-* --output-dir conda-bld/
      - anaconda --token=$ANACONDA_TOKEN --verbose upload conda-bld/linux-64/package-name-* --user mosaic
      - anaconda --token=$ANACONDA_TOKEN --verbose upload conda-bld/osx-64/package-name-* --user mosaic
      - anaconda --token=$ANACONDA_TOKEN --verbose upload conda-bld/win-64/package-name-* --user mosaic
      only:
      - main
      - develop
    ```

* Replace the `package-name` by the name of your package.

* Commit and push your changes.


## Check the pipeline status of Gitlab

* On GitLab, using the left menu, go to `CI/CD > Pipelines`

* Click on the status button (`running` for instance) of the last pipeline to monitor the status

* In case of a `Runner System Failure`, press Retry.