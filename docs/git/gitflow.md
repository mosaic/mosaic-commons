# Git-Flow

## Install Git-Flow

=== "macOS"
    ```zsh
    brew install git-flow-avh
    ```
=== "Linux"
    ```bash
    apt-get install git-flow
    ```

### Initialize Git-Flow

To initialize Git-Flow, inside an existing git repo: 
```bash
git flow init
```

**It is highly advised to follow standard branch names!**

## Get familiar with Git-Flow commands

Get familiar with the features `start`, `finish` & `publish` commands.

![](https://danielkummer.github.io/git-flow-cheatsheet/img/git-flow-commands.png)

- [English cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.html)
- [French cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)
