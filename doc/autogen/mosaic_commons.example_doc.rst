mosaic\_commons.example\_doc
============================

.. automodule:: mosaic_commons.example_doc

   
   
   .. rubric:: Functions

   .. autosummary::
   
      myfunc
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      MyObj
   
   

   
   
   