Write a Sphinx documentation
--------------------------------

Document your Python modules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- NumPy style docstrings for classes and functions

.. note::
    numpydoc provides an updated `documentation of how to format docstrings <https://numpydoc.readthedocs.io/en/latest/format.html>`__ and a `complete example <https://numpydoc.readthedocs.io/en/latest/example.html#example>`__.


- Use `typehints <https://peps.python.org/pep-0484/>`__ when possible

.. hint:: Basic example:

    .. code-block:: python

        def koch_snowflake(order: int, scale: float=10.):
            """Return two lists of point coordinates of the Koch snowflake.

            The function computes a Koch snowflake of a given order and base
            edge length, and returns the x, y coordinates of the points
            describing the snowflake edges.

            Parameters
            ----------
            order : int
                The recursion depth.
            scale : float
                The extent of the snowflake (edge length of the base triangle).

            Returns
            -------
            x : array_like
                The x coordinates of the snowflake points.
            y : array_like
                The y coordinates of the snowflake points.
            """

            x, y = ...
            return x, y

- Module header: `shebang`, encoding + copyright mention?

.. code-block:: python

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    # ------------------------------------------------------------------------------
    #  Copyright (c) 2022 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
    #  All rights reserved.
    #  This file is part of the my_package library, and is released under the
    #  "LGPLv3" license. Please see the LICENSE file that is included as part of
    #  this package.
    # ------------------------------------------------------------------------------

.. warning::
    There is no defined standard in the team on the format of the module header.

Set up Sphinx for auto-documentation of source code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  ``conda install -c conda-forge sphinx myst-parser``

-  Go to the root of your package and use ``sphinx-quickstart`` to generate doc files in a ``doc/`` folder:

.. code-block:: bash

   sphinx-quickstart --suffix .md doc/

- Do not separate source/ and build/ ?
- Fill in project name, author and version when asked

Edit the files in ``doc/``
^^^^^^^^^^^^^^^^^^^^^^^^^^

- Edit the ``index.md`` into proper Markdown, e.g.:

.. code-block:: markdown

    # Welcome to `project_name`'s documentation!

- Edit the ``conf.py`` to add the `MyST Parser <https://myst-parser.readthedocs.io/en/latest/index.html>`__ extension

.. code-block:: python

    # Add any Sphinx extension module names here, as strings. They can be
    # extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
    # ones.
    extensions = [
        'myst_parser'
    ]

- Enable the LaTeX support and triple-colon ("colon-fence") `optional MyST syntax <https://myst-parser.readthedocs.io/en/latest/syntax/optional.html>`__

.. code-block:: python

    # -- MyST options ------------------------------------------------------------

    myst_enable_extensions = ["colon_fence", "amsmath", "dollarmath"]

- Extra options: header depth up to which generate anchors, allow equation labels in dollar equations?

.. code-block:: python

    myst_heading_anchors = 2
    myst_dmath_allow_labels = True


Compile the documentation and check it
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  Run the compilation

.. code-block:: bash

   cd doc/
   make html

-  Open the ``build/html/index.html`` page in a browser

Add additional doc files and reference them
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- For instance add an ``install.md`` file

    .. code-block:: markdown

        # Installation instructions

        ## Developer procedure

        * Clone the package sources

        ```bash
        git clone https://gitlab.inria.fr/mosaic/package_name.git
        ```

        * Create a new conda environment

        ```bash
        conda env create -f conda/env.yaml
        ```

        * Activate the environment and install the package

        ```bash
        conda activate package_name
        python -m pip install -e .
        ```

- Add the ``install`` file in the ``index.md`` `table of contents <https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#table-of-contents>`__. ``toctree`` being a RST directive parsed by `MyST`, it can be used with the colon-fence syntax.

    .. code-block:: markdown

        :::{toctree}
        :maxdepth: 2

        install
        :::


Automatic source doc generation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Sphinx provides tools to automatically render (well-documented) classes and modules in html: the `Autodoc extension <https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html>`__.

- Add  ``autodoc`` to the extensions in the ``conf.py``. If your dosctrings are following `NumPy style <https://numpydoc.readthedocs.io/en/latest/format.html>`__ you will also need the ``napoleon`` extension to parse them into ReStructured Text.

.. code-block:: markdown

    extensions = [
        'myst_parser',
        'sphinx.ext.autodoc',
        'sphinx.ext.napoleon',
    ]

- In ``conf.py`` append your source directory to the path

.. code-block:: python

    # -- Path setup --------------------------------------------------------------

    # If extensions (or modules to document with autodoc) are in another directory,
    # add these directories to sys.path here. If the directory is relative to the
    # documentation root, use os.path.abspath to make it absolute, like shown here.
    #
    import os
    import sys
    sys.path.insert(0, os.path.abspath('../src'))

- To remove module names in docstrings you can add:

.. code-block:: python

    # If true, the current module name will be prepended to all description
    # unit titles (such as .. function::).
    add_module_names = False

.. note::

    You can now directly insert the documentation of a given module / class using the ``automodule`` / ``autoclass`` directives in your ``.md`` source files. ``automodule`` / ``autoclass`` being non-native RST directives, they won't be parsed by `MyST` annd .

    .. code-block:: markdown

        :::{eval-rst}
        .. automodule:: package_name.module
           :members:
        :::

- Sphinx also provides `Apidoc <https://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html>`__, a command-line interface to automatically generate ``.rst`` files with ``automodule`` / ``autoclass`` directives for all the modules in your source directory. It can be executed from Python, and thus conveniently added to the ``conf.py``:

.. code-block:: python

    # -- API-doc -----------------------------------------------------------------

    # Use apidoc main function to automatically generate .rst sources of all modules
    # for the developer documentation.
    #
    api_doc_path = '_api'

    from sphinx.ext.apidoc import main
    destdir = os.path.abspath(api_doc_path)
    if not os.path.isdir(destdir):
        os.makedirs(destdir)
    main(['-f', '-o', destdir, '-e', os.path.abspath('../src')])

- Insert a link to the module index or to any generated API page in your ``.md`` source files.

.. code-block:: markdown

    [API documentation](py-modindex)


.. code-block:: markdown

    :::{toctree}
    :maxdepth: 2

    install
    _api/package_name
    :::


Link to other Sphinx documentations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Add the ``intersphinx`` extension in the ``conf.py``

.. code-block:: python

    extensions = [
        ...,
        'sphinx.ext.intersphinx',
        ...,
    ]

- In ``conf.py`` add the mappings with other Sphinx documentations

.. code-block:: python

    # -- Intersphinx -------------------------------------------------------------
    # Configuration for intersphinx: refer to the Python standard library.
    intersphinx_mapping = {
        'python': ('https://docs.python.org/3', None),
        'pandas': ('https://pandas.pydata.org/pandas-docs/stable/', None),
        'numpy': ('https://numpy.org/doc/stable/', None),
        'matplotlib': ('https://matplotlib.org/stable/', None),
        'scipy': ('https://docs.scipy.org/doc/scipy/', None)
    }

Choose Sphinx theme
^^^^^^^^^^^^^^^^^^^

- Modify the ``html_theme`` in ``conf.py`` using a theme from the `Sphinx themes gallery <https://sphinx-themes.org/>`__

.. code-block:: python

    # The theme to use for HTML and HTML Help pages.  See the documentation for
    # a list of builtin themes.
    html_theme = "nature"

.. warning::

    In most cases, an installation is required to use the chosen theme. Simply follow the guidelines provided in the documentation page of the theme.

    .. code-block:: bash

        pip install sphinx-press-theme

    .. code-block:: python

        # The theme to use for HTML and HTML Help pages.  See the documentation for
        # a list of builtin themes.
        html_theme = 'press'

Extra directives using sphinx-design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- `Sphinx{design} <https://sphinx-design.readthedocs.io/en/latest/>`__ is an external sphinx extension that provides a lot of interesting web components accessible as colon-fence directives in your ``.md`` source files.

- Install ``sphinx-design`` and add it to the extensions in ``conf.py``

.. code-block:: bash

    pip install sphinx-design


.. code-block:: python

    extensions = [
        ...,
        "sphinx_design",
        ...,
    ]

- Examples of web components include `Cards <https://sphinx-design.readthedocs.io/en/latest/cards.html#cards>`__, `Grids <https://sphinx-design.readthedocs.io/en/latest/grids.html#placing-a-card-in-a-grid>`__, `Dropdowns <https://sphinx-design.readthedocs.io/en/latest/dropdowns.html#dropdowns>`__ or `Carousels <https://sphinx-design.readthedocs.io/en/latest/cards.html#card-carousels>`__.

Add a logo on every documentation page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  Edit the ``conf.py``

::

   html_logo = "_static/path/to/logo.png"

-  You may edit other “html” variables to further customize doc pages

