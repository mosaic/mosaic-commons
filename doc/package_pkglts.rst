.. warning::

    This section is outdated!

Package your source code using ``pkglts``
---------------------------------------------

Install the ``pkglts`` tool
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   conda install -c revesansparole pkglts

Initialize the package structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  Create an empty package directory

-  Run the initiation commands for ``pkglts``

::

   pmg init
   pmg add base pysetup

-  Edit the ``.pkglts/pkg_cfg.json``

   -  Modify author name(s) + adress(es)
   -  Leave ``null`` namespace?
   -  Modify description

-  Generate the package structure

::

   pmg rg

Transfer the sources in ``src/``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  Add an empty ``__init__.py`` in each directory

Install your package
^^^^^^^^^^^^^^^^^^^^

::

   python setup.py develop

-  Test that the import is working



