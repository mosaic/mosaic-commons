Package your source code
------------------------


Initialize the package structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Create an empty package directory with your package name

- Create a ``src/`` directory, with an empty directory with your package name

- Create a ``README.md`` file with the minimal information:

    * package name
    * short description of functionalities
    * list of authors / contributors
    * installation procedure

- Create a ``setup.py`` file with the following content (replacing with actual info where relevant)

.. code-block:: python

    #!/usr/bin/env python
    #-*- coding: utf-8 -*-

    from setuptools import setup, find_packages

    short_descr = "Short description of your package"
    readme = open("README.md")

    # find packages
    pkgs = find_packages('src')

    setup_kwds = dict(
        name='package_name',
        version="X.X.X",
        description=short_descr,
        long_description=readme,
        author="Author Name",
        author_email="author.name@email.com",
        url='',
        license='LGPL-3.0',
        zip_safe=False,

        packages=pkgs,

        package_dir={'': 'src'},
        entry_points={},
        keywords='',
    )

    setup(**setup_kwds)

- Create a ``pyproject.toml`` with the following content:

.. code-block:: toml

    [build-system]
    requires = ["setuptools"]
    build-backend = "setuptools.build_meta"

- Add a ``LICENSE`` file copying the text of the chosen license (typically `LGPL-3.0 <https://opensource.org/licenses/LGPL-3.0>`__):

Transfer the sources in ``src/``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  Add an empty ``__init__.py`` in each directory

Install your package
^^^^^^^^^^^^^^^^^^^^

- Install in edit mode with the ``-e`` option to be able to use source updates without re-installing:

.. code-block:: bash

   python -m pip install -e .

-  Test that the import is working (replacing ``<my_package>`` by its actual name):

.. code-block:: bash

   python -c "import <my_package>"

