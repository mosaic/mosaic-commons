============
Unit testing
============

Add a test directory to host unit tests
=======================================

* Create a `test/` directory at the root of the package

Create a test python module for each module of the package
==========================================================

* Create a Python file named `test_$$$.py` in `test/`

* Create a test class that inherits from  `unittest.TestCase <https://docs.python.org/3/library/unittest.html#unittest.TestCase>`__

* Define the `setUp` and `tearDown` functions

.. code-block:: python

    import unittest

    class Test$$$(unittest.TestCase):

        def setUp(self):
            """
            Define class members that will be used for all tests
            """
            pass

        def tearDown(self):
            """
            Cleanup after the execution of every test method
            """
            pass

* Write one test method (beginning by `test_`) per testing case

.. code-block:: python

        def test_$$$(self):
            """
            Run some functions of the package
            """

            assert #Some condition

* You can use the built-in methods from the `unittest.TestCase` class (`self.assertAlmostEqual`...)

Run the tests
=============

With Nose2
----------

* Install the `nose2` testing suite, with the `coverage` package

::

    conda install -c conda-forge nose2 coverage

* Add the coverage configuration lines in the `pyproject.toml`: avoid to count useless .py files in coverage stats

::

    [tool.coverage.run]
    source = ["src"]
    omit = ["*__init__.py", "test/*", "setup.py"]

    [tool.coverage.report]
    omit = ["*__init__.py", "test/*", "setup.py"]

* Add a `unittest.cfg` file at the root of the package

::

    [unittest]
    test-file-pattern = test_*.py

    [coverage]
    always-on = True

* At the root of the package, run the testing command to see the percentage of coverage

::

    nose2 -v


With PyTest
-----------

* Install the `pytest` testing suite, with the coverage extension

::

    conda install pytest pytest-cov


* At the root of the package, run the testing command to see the percentage of coverage

::

    pytest -v --cov src/ test/

