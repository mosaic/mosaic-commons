
How to develop and release a Python package
-------------------------------------------

.. toctree::
   :maxdepth: 2

   package_init
   gitlab
   gitflow
   test
   doc
   notebooks
   conda
   gitlab-ci
